FROM onjin/alpine-postgres:9.6.2
MAINTAINER Rustem Uspanov <ruspanov@sanscrit.kz>

ENV TIMEZONE            Asia/Almaty
COPY docker-entrypoint.sh /
RUN apk update && \
    apk upgrade && \
    apk --update add tzdata && \
    rm -rf /etc/localtime && \
    cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && \
    echo "${TIMEZONE}" > /etc/timezone && \
    apk del tzdata && \
	rm -rf /var/cache/apk/*

ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 5432
CMD ["postgres"]	